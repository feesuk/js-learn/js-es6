// string
let sayHi = "Hallo Semua,";
let fullName = "Kim Jong Un";
let domicile = "Pyonyang";

function sayThanks() {
    return "Thanks All";
}

//implement of template literal
let greeting = `${sayHi ? sayHi : ""}
Nama saya adalah ${fullName},
saya berdomisili di ${domicile},
sekian perkenalan diri saya.
${sayThanks()}
`;

console.log(greeting);

/**
 * Benefit of Template Literal
 *
 1 - Miltiple line of sring
 2 - Interpolation / penyisipan
 */

/**
 * macam-macam slash
 / = slash
 \ = back slash
 */
