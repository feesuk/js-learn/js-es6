import { sayHi, sayBye } from './functionLib.js';

sayHi('John'); // Hello, John!
sayBye('John'); // Bye, John!