function greeting(name = "unindetify") {
    return "My is " + name;
}

//contoh standar dari arrow function
sayHi = (name = "unindetify", age) => {
    return `Hi, my name is ${name}, I'm ${age} year old`;
};


//arrow function yang hanya punya satu parameter itu boleh gak pake kurung
sayHi2 = name => {
    return `Hi, my name is ${name}`;
};

//arrow function return
sayHi3 = name => (
    `Hi, my name is ${name}`
);

console.log(sayHi3("Budi"));
