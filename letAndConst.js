/**
 * =====================
 * Let Vs Var
 * =====================
 * Let : punya block scope
 * Var : tidak punya block scope
 * Keduanya adalah variabel mutable = bisa diubah
 */

//global variable with var
var fullName = "Masagus Hariadi Arief";
//global variable with let
let address = "Depok";

// if adalah contoh block scope
if (fullName) {
  //local variable with var
  var job = "Front-end Dev";
  //local variable with let
  let sideJob = "Binar Facil";
}

//  console.log(fullName); // output : Masagus Hariadi Arief
//  console.log(address); // output : Depok
//  console.log(job); // output : Front-end Dev
//  console.log(sideJob); // output : err

/**
 * const adalah variabel re-assgin, tapi tidak berarti bahwa const adalah immutable --walaupun dibeberapa dokumentasi disebutkan bahwa const adalah immutable--.
 * data object yang di deklarasikan dengan const tetap dapat dimodifikasi
 */

let buah1 = "apel";
let buah2 = buah1;
buah1 = "jeruk";
// console.log(buah2); output : apel -> karena tipe data primitif

const person1 = {
  name: "Kim Jong Un",
  gender: "Laki-laki",
};

/**
 *  const tidak bisa di re-assign karena immutable
 */
// orang = {
//   nama: "Donal Trump",
//   gender: "Laki-laki",
// };

/**
 * const yang memiliki data object, objectnya bisa diubah
 */
// person1.name = "Donal Trump";
// person1.age = "21";

const person2 = person1;
person1.name = "Donal Trump";
// console.log(person2.name); output : Donal Trump -> karena tipe datanya object, yang mana disimpan dimemori berdasarkan referensi
