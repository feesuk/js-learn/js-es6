let biodata = {
    nama: "Budi",
    umur: 21,
    jenisKelamin: 'Laki-laki',
    address: {
        city: 'Palembang',
        state: 'palembang Selatan'
    }
}

const { nama, umur, address } = biodata // same as -> const nama = biodata.nama
const { city, state } = address

console.log(nama);
console.log(umur);

// =======================

const person = {
    nama: 'Budi',
    umur: 21
}


function greeting(person) {
    const { nama, umur } = person
    //ternary-condition ? if true : else
    return person.nama ? `Hi, nama saya ${nama} umur saya ${umur}` : 'Tidak ada orang'
    /**
     * sama dengan :
        if (person) {
            return `Hi, nama saya ${person}`
        } else {
            return 'Tidak ada orang'
        }
     */

}

console.log(greeting(person));