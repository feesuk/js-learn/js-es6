let students = [
    { name: 'budi', age: 21 },
    { name: 'seno', age: 18 },
    { name: 'santo', age: 15 },
    { name: 'bambang', age: 8 },
]

let studentsnameList = students.map((student, index) => {
    return `idex ke=${index} adalah ${student.name}`
})

let adultStudentList = students.filter((student) => (
    student.age >= 17
))

//combine map and filter
let adultStudentOnlyNameList = students.filter((student) => (
    student.age >= 17
)).map((student) => student.name)


let adultStudent = students.find((student) => (
    student.age >= 17
))

let numbers = [1, 2, 3, 4, 5, 6]
let numberGreatherThanTree = numbers.find((number) => number > 3)


console.log(numberGreatherThanTree);